//Obtener el objeto button de calcular

const btnCalcular = document.getElementById('btnCalcular');
const btnLimpiar = document.getElementById("btnLimpiar");

btnCalcular.addEventListener('click', function(){
    let valorAuto = document.getElementById('valorAuto').value;
    let porcentaje = document.getElementById('Porcentaje').value;
    let plazo = document.getElementById('plazos').value;

    //hacer los calculos
    let pagoInicial = valorAuto*(porcentaje/100);
    let totalFin = valorAuto-pagoInicial;
    let plazos = totalFin/plazo;

    //Mostrar los datos

    document.getElementById('pagoInicial').value = pagoInicial.toFixed(2);
    document.getElementById('totalFin').value = totalFin.toFixed(2);
    document.getElementById('pagoMensual').value = plazos.toFixed(2);
});

//codificar boton limpiar
btnLimpiar.addEventListener("click", function(){
    const valorAuto = document.getElementById("valorAuto");
    const porcentaje = document.getElementById("Porcentaje");
    const plazos = document.getElementById("plazos");
    const pagoInicial = document.getElementById("pagoInicial");
    const totalFin = document.getElementById("totalFin");
    const pagoMensual = document.getElementById("pagoMensual");

    valorAuto.value = "";
    porcentaje.value = "";
    plazos.value = "12"; 
    pagoInicial.value = "";
    totalFin.value = "";
    pagoMensual.value = "";
  });
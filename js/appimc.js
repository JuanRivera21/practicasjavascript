const botoncalc = document.getElementById('btnCalcular');

btnCalcular.addEventListener('click', function(){
    let altura = document.getElementById('altura').value;
    let peso = document.getElementById('peso').value;
    let imctotal = peso / Math.pow(altura,2);

    document.getElementById('imc').value = imctotal.toFixed(2);
    Imagen(imctotal);
    Calorias(peso);
});

function Imagen(imc){
let img = document.getElementById('imagen');
let sexoM = document.getElementById('Hombre').checked;

//console.log(sexo);
if(imc<18.5){
    if(sexoM){
        img.src="../img/01H.png"
    } else {
        img.src="../img/01M.png"
    }
}else if(imc>=18.5 && imc<=24.9 ){
    if(sexoM){
        img.src="../img/02H.png"
    }else{
        img.src="../img/02M.png"
    } 
}else if(imc>24.9 && imc<=29.9){
    if(sexoM){
        img.src="../img/03H.png"
    }else{
        img.src="../img/03M.png"
    } 
}else if(imc>29.9 && imc<=34.9){
    if(sexoM){
        img.src="../img/04H.png"
    } 
    else{
        img.src="../img/04M.png"
    } 
}else if(imc>34.9 && imc<=39.9){
    if(sexoM){
        img.src="../img/05H.png"
    } 
    else{
        img.src="../img/05M.png"
    } 
}else if(imc>=40){
    if(sexoM){
        img.src="../img/06H.png"
    } 
    else{
        img.src="../img/06M.png"
    } 
}
}

function Calorias(peso){
    let edad = document.getElementById('Edad').value;
    let sexo = document.getElementById('Hombre').checked;

    if(edad>=10 && edad<18){
        if(sexo){
            document.getElementById('calorias').value= 17.686*peso+658.2;
        }else{
            document.getElementById('calorias').value= 13.384*peso+692.6;
        }
    }else if(edad>=18 && edad<30){
        if(sexo){
            document.getElementById('calorias').value= 15.057*peso+692.2;
        }else{
            document.getElementById('calorias').value= 14.818*peso+486.6;
        }
    }else if(edad>=30 && edad<60){
        if(sexo){
            document.getElementById('calorias').value= 11.472*peso+873.1; 
        }else{
            document.getElementById('calorias').value= 8.126*peso+845.6;
        }
    }else if(edad>=60){
        if(sexo){
            document.getElementById('calorias').value= 11.711*peso+587.7;
        }else{
            document.getElementById('calorias').value= 9.082*peso+658.5;
        }
    }

}
